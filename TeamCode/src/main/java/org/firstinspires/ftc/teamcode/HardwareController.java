package org.firstinspires.ftc.teamcode;

/**
 * Created by nicolas on 1/28/18 in ftc_app.
 * <p>
 * Copyright (c) ©2018 Nicolas Hohaus
 * Copyright (c) ©2018 Walbots (7175)
 * <p>
 * Resource: https://gitlab.com/roboticsclub/ftc_app
 * Contact: nico@walbots.com, team@walbots.com
 */


import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DcMotor;

import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.HardwareMap;

import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.Range;

import org.firstinspires.ftc.teamcode.Walbots7175.internal.LoggingController;


/**
 * The class HardwareController is controlling the complete hardware of the robot.
 * The hardware properties are set to be private enforcing to use the defined methods
 * to set/get properties of the hardware. On this basic hardware level we control custom
 * specialities of different kind of hardware so that we do not have to take care about those in
 * other places in the code.
 *
 * For example: We enable motors to turn in reverse by having a negative power set. We care about
 * the motor turning FORWARD or BACKWARD in this controller, so that we can just set a positive or
 * negative power somewhere else and don't have to worry about all specialities everywhere in the
 * code.
 *
 * - Walbots
 */
public class HardwareController
{
    //----------------------------------------------------------------------------------------------
    // Static Properties

    public static final float MIN_MOTOR_POWER = 0.1f;//The lowest power that can be set for motors



    //----------------------------------------------------------------------------------------------
    // Private Properties
    private HardwareMap hardwareMap;        //Hardware map for accessing the current hardware



    //----------------------------------------------------------------------------------------------
    // Hardware Input (private!!!)

    //TODO: Define all input hardware (all sensors)
    private ColorSensor colorSensor;




    //----------------------------------------------------------------------------------------------
    // Hardware Output (private!!!)

    //TODO: Define all output hardware (all motors, servos)
    private DcMotor leftDrive;
    private DcMotor rightDrive;




    //----------------------------------------------------------------------------------------------
    // Initialization
    /**
     * Initializes a HardwareController with the HardwareMap from the running OpMode.
     * This class provides functionality to access and control the hardware of the robot.
     *
     * @param hardwareMap The HardwareMap of the currently running OpMode
     */
    public HardwareController(HardwareMap hardwareMap)
    {
        this.hardwareMap = hardwareMap;

        //Input
        //TODO: Set all Input hardware
        colorSensor = (ColorSensor)getHardware(ColorSensor.class, "colorSensor");


        //Output
        //TODO: Set all Output hardware
        leftDrive = getDcMotor("leftMotor");
        rightDrive = getDcMotor("rightMotor");

    }



    //----------------------------------------------------------------------------------------------
    // Sensors (Color, Range etc)

    //TODO: Define all methods to access the hardware sensors (getColor, getDistance, etc)

    public void turnColorSensorLEDOn(boolean turnOn)
    {
        colorSensor.enableLed(turnOn);
    }

    public int getColorSensorBlueValue()
    {
        return colorSensor.blue();
    }



    //----------------------------------------------------------------------------------------------
    // Motors and Servos

    //TODO: Define all methods to power the hardware (powerMotor, powerServo, etc)

    public void powerLeftDrivingMotor(float power)
    {
        powerMotor(leftDrive, -power);  //Negate power because motor is turned other direction
    }

    public void powerRightDrivingMotor(float power)
    {
        powerMotor(rightDrive, power);
    }




    //----------------------------------------------------------------------------------------------
    // Helper functions

    /**
     * The getDcMotor() method looks for a DcMotor with the given name and returns it if available.
     *
     * @param name The DcMotor's name defined in the robot configuration on the user phone
     * @return If available the DcMotor object
     */
    private DcMotor getDcMotor(String name)
    {
        return (DcMotor) getHardware(DcMotor.class, name);
    }

    /**
     * The getServo() method looks for a Servo with the given name and returns it if available.
     *
     * @param name The Servo's name defined in the robot configuration on the user phone
     * @return If available the Servo object
     */
    private Servo getServo(String name)
    {
        return (Servo) getHardware(Servo.class, name);
    }

    /**
     * The getHardware() method looks for the specified hardware and returns it if available.
     *
     * @param hardwareType The type of hardware to look for (eg DcMotor.class)
     * @param name The hardware's name defined in the robot configuration on the user phone
     * @return If available the hardware object
     */
    private Object getHardware(Class hardwareType, String name)
    {
        Object hardware = null;
        try
        {
            hardware = hardwareMap.get(hardwareType, name);
        }catch (NullPointerException e)
        {
            LoggingController.getCurrentInstance().showLog("Hardware", "Null Pointer Exception");
        }

        return hardware;
    }

    /**
     * The isAvailable() method checks if a given hardware (or object) is currently available.
     *
     * @param hardware Hardware to check it's availability
     * @return Whether the hardware is available or not
     */
    private boolean isAvailable(Object hardware)
    {
        return hardware != null;
    }

    /**
     * The powerMotor() method sets power to a given motor. It translates the power value [-1,+1]
     * into a from the motor understandable set of instructions. Only stressing the motor without
     * it being able to move (because of too less power) is prevented by this method.
     *
     * @param motor The motor which will be powered (moved)
     * @param power The power value to power the motor with (from -1 until +1, 0=stop)
     */
    private void powerMotor(DcMotor motor, float power)
    {
        if(!isAvailable(motor)) //Just return if motor not available
        {
            return;
        }

        //Set direction
        motor.setDirection(power >= 0 ? DcMotorSimple.Direction.FORWARD : DcMotorSimple.Direction.REVERSE);

        //Set power
        float outputPower = Math.abs(power);

        if (outputPower > 0)
        {
            outputPower = Math.max(MIN_MOTOR_POWER, power);
        }

        motor.setPower(outputPower);
    }

    /**
     * The powerServo() method sets power to a given servo. It translates the power value into a
     * from the servo understandable set of instructions. For servos the power value can out of the
     * range of -1 until +1 enabling the servo to move faster, because setting the power to 1 is
     * equal to moving the servo by 1 degree for each time this function is called. So In order to
     * have the servo continually moving this method has to be repeatedly called.
     *
     * @param servo The servo which will be powered (moved)
     * @param power The power value to power the servo with (can be out of the range from -1 to +1)
     */
    private void powerServo(Servo servo, double power)
    {
        if(!isAvailable(servo)) //Just return if servo not available
        {
            return;
        }

        //Scale power
        power = Range.scale(power, 0, 360, 0, 1);

        //Calculate new position
        double newPosition = Range.clip(servo.getPosition() + power, 0, 1);

        //Set new position
        servo.setPosition(newPosition);
    }
}
